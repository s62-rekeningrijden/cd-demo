package eu.rekeningrijden.cddemo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CdDemoApplication

fun main(args: Array<String>) {
    runApplication<CdDemoApplication>(*args)
}
